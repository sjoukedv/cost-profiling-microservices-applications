from locust import HttpUser, between, task

class NextUser(HttpUser):
    host = "https://agents.domain.dev:3002"
   
    wait_time = between(5, 15)
        
    # Nextjs RUM
    # NOTE rum doesn't show in these kind of tests
    @task
    def pages(self):
        self.client.get("/")
        self.client.get("/posts/dynamic-routing")
        self.client.get("/posts/preview")
        self.client.get("/posts/hello-world")

class NodeUser(HttpUser):
    host = "https:///agents.domain.dev:3000"
   
    wait_time = between(5, 15)
        
    # NodeJS express
    @task
    def pages(self):
        self.client.get("/")
        self.client.get("/users")
        self.client.get("/user/0")
        self.client.get("/user/0/edit")
        self.client.get("/pet/0")
        self.client.get("/pet/1")
        self.client.get("/pet/1/edit")
        self.client.get("/pet/2")


class RubyUser(HttpUser):
    host = "https:///agents.domain.dev:3001"
   
    wait_time = between(5, 15)
        
    # Ruby
    @task
    def pages(self):
        self.client.get("/")        