#!/bin/bash
# get APM secret token to connect agents
ELASTIC_APM_SECRET_TOKEN=$(kubectl get secret/quickstart-apm-token -o go-template='{{index .data "secret-token" | base64decode}}')
ELASTIC_APM_SERVER_URL="https://apm.sdvontwikkeling.nl:8200"

ELASTIC_APM_SERVICE_NAME="example-ruby" \
ELASTIC_APM_VERIFY_SERVER_CERT=true \
ELASTIC_APM_SECRET_TOKEN=$ELASTIC_APM_SECRET_TOKEN \
ELASTIC_APM_SERVER_URL=$ELASTIC_APM_SERVER_URL \
 bin/rails server -p 3001