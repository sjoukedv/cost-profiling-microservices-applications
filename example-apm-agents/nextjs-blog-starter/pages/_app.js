import '../styles/index.css'

import { init as initApm } from '@elastic/apm-rum'

const apm = initApm({
  // Set required service name (allowed characters: a-z, A-Z, 0-9, -, _, and space)
  serviceName: process.env.NEXT_PUBLIC_ELASTIC_APM_SERVICE_NAME,

  verifyCert: process.env.NEXT_PUBLIC_ELASTIC_APM_VERIFY_SERVER_CERT,

  // Set custom APM Server URL (default: http://localhost:8200)
  serverUrl: process.env.NEXT_PUBLIC_ELASTIC_APM_SERVER_URL || 'https://localhost:8200',
  secretToken: process.env.NEXT_PUBLIC_ELASTIC_APM_SECRET_TOKEN,

  environment: process.env.NODE_ENV

  // Set service version (required for sourcemap feature)
  // serviceVersion: ''
})

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
