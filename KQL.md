Hidden indices in `.ds-*`

`agent.type: "metricbeat" AND metricset.name:"node" ` shows

```
{
  "_index": ".ds-metricbeat-8.1.0-2022.03.17-000001",
  "_id": "vPKpl38BchZ5pvpwjUZE",
  "_version": 1,
  "_score": 1,
  "_source": {
    "@timestamp": "2022-03-17T11:35:45.664Z",
    "cloud": {
      "service": {
        "name": "Droplets"
      },
      "instance": {
        "id": "291075976"
      },
      "provider": "digitalocean",
      "region": "ams3"
    },
    "service": {
      "type": "kubernetes",
      "address": "https://autoscale-worker-pool-cnvwt:10250/stats/summary"
    },
    "event": {
      "dataset": "kubernetes.node",
      "module": "kubernetes",
      "duration": 962295
    },
    "metricset": {
      "period": 10000,
      "name": "node"
    },
    "kubernetes": {
      "node": {
        "cpu": {
          "usage": {
            "nanocores": 112841047,
            "core": {
              "ns": 1560023696486
            }
          }
        },
        "network": {
          "rx": {
            "bytes": 892901470,
            "errors": 0
          },
          "tx": {
            "bytes": 183431924,
            "errors": 0
          }
        },
        "runtime": {
          "imagefs": {
            "available": {
              "bytes": 71280771072
            },
            "capacity": {
              "bytes": 84514574336
            },
            "used": {
              "bytes": 4135706624
            }
          }
        },
        "name": "autoscale-worker-pool-cnvwt",
        "memory": {
          "pagefaults": 50886,
          "majorpagefaults": 132,
          "available": {
            "bytes": 2619756544
          },
          "usage": {
            "bytes": 3534573568
          },
          "workingset": {
            "bytes": 1504595968
          },
          "rss": {
            "bytes": 717672448
          }
        },
        "fs": {
          "capacity": {
            "bytes": 84514574336
          },
          "used": {
            "bytes": 9759973376
          },
          "inodes": {
            "free": 5075132,
            "count": 5242880,
            "used": 167748
          },
          "available": {
            "bytes": 71280771072
          }
        },
        "start_time": "2022-03-17T08:06:03Z"
      },
      "labels": {
        "topology_kubernetes_io/region": "ams3",
        "kubernetes_io/hostname": "autoscale-worker-pool-cnvwt",
        "doks_digitalocean_com/node-pool": "autoscale-worker-pool",
        "node_kubernetes_io/instance-type": "s-2vcpu-4gb",
        "beta_kubernetes_io/instance-type": "s-2vcpu-4gb",
        "kubernetes_io/arch": "amd64",
        "doks_digitalocean_com/node-id": "d5819820-b4ef-4deb-83d9-bb8e049ea2cf",
        "doks_digitalocean_com/node-pool-id": "f68680a4-c692-4057-84c0-710141e70d1e",
        "kubernetes_io/os": "linux",
        "doks_digitalocean_com/version": "1.22.7-do.0",
        "region": "ams3",
        "failure-domain_beta_kubernetes_io/region": "ams3",
        "beta_kubernetes_io/os": "linux",
        "beta_kubernetes_io/arch": "amd64"
      }
    },
    "host": {
      "ip": [
        "164.92.151.234",
        "10.18.0.7",
        "fe80::4c0e:efff:fe96:f629",
        "10.110.0.6",
        "fe80::dc05:28ff:fe8c:b521",
        "172.17.0.1",
        "fe80::b45e:11ff:fef8:63c3",
        "10.244.1.41",
        "fe80::5c6a:b5ff:fea2:85aa",
        "fe80::1c62:9eff:fef1:7b28",
        "fe80::bc20:8fff:fefd:d62e",
        "fe80::b433:5ff:feb2:6188",
        "fe80::a0f6:27ff:feff:50c9"
      ],
      "mac": [
        "4e:0e:ef:96:f6:29",
        "de:05:28:8c:b5:21",
        "02:42:60:f4:7c:07",
        "b6:5e:11:f8:63:c3",
        "5e:6a:b5:a2:85:aa",
        "1e:62:9e:f1:7b:28",
        "be:20:8f:fd:d6:2e",
        "b6:33:05:b2:61:88",
        "a2:f6:27:ff:50:c9"
      ],
      "hostname": "autoscale-worker-pool-cnvwt",
      "name": "autoscale-worker-pool-cnvwt",
      "architecture": "x86_64",
      "os": {
        "codename": "focal",
        "type": "linux",
        "platform": "ubuntu",
        "version": "20.04.3 LTS (Focal Fossa)",
        "family": "debian",
        "name": "Ubuntu",
        "kernel": "5.10.0-0.bpo.9-amd64"
      },
      "containerized": true
    },
    "agent": {
      "id": "e97f15cc-51d4-47f5-b97c-9b4a008db2ad",
      "name": "autoscale-worker-pool-cnvwt",
      "type": "metricbeat",
      "version": "8.1.0",
      "ephemeral_id": "1dab97b7-4f18-4091-b852-d7a7e8e8660c"
    },
    "ecs": {
      "version": "8.0.0"
    }
  },
  "fields": {
    "host.os.name.text": [
      "Ubuntu"
    ],
    "host.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.labels.kubernetes_io/arch": [
      "amd64"
    ],
    "host.mac": [
      "4e:0e:ef:96:f6:29",
      "de:05:28:8c:b5:21",
      "02:42:60:f4:7c:07",
      "b6:5e:11:f8:63:c3",
      "5e:6a:b5:a2:85:aa",
      "1e:62:9e:f1:7b:28",
      "be:20:8f:fd:d6:2e",
      "b6:33:05:b2:61:88",
      "a2:f6:27:ff:50:c9"
    ],
    "kubernetes.node.runtime.imagefs.available.bytes": [
      71280771072
    ],
    "kubernetes.labels.doks_digitalocean_com/node-pool-id": [
      "f68680a4-c692-4057-84c0-710141e70d1e"
    ],
    "service.type": [
      "kubernetes"
    ],
    "kubernetes.labels.beta_kubernetes_io/instance-type": [
      "s-2vcpu-4gb"
    ],
    "host.os.version": [
      "20.04.3 LTS (Focal Fossa)"
    ],
    "kubernetes.labels.doks_digitalocean_com/node-id": [
      "d5819820-b4ef-4deb-83d9-bb8e049ea2cf"
    ],
    "kubernetes.labels.doks_digitalocean_com/version": [
      "1.22.7-do.0"
    ],
    "kubernetes.labels.beta_kubernetes_io/arch": [
      "amd64"
    ],
    "host.os.name": [
      "Ubuntu"
    ],
    "kubernetes.labels.kubernetes_io/hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "agent.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "host.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "beats_state.state.host.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "cloud.region": [
      "ams3"
    ],
    "host.os.type": [
      "linux"
    ],
    "kubernetes.labels.failure-domain_beta_kubernetes_io/region": [
      "ams3"
    ],
    "kubernetes.node.runtime.imagefs.capacity.bytes": [
      84514574336
    ],
    "agent.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "host.architecture": [
      "x86_64"
    ],
    "kubernetes.labels.node_kubernetes_io/instance-type": [
      "s-2vcpu-4gb"
    ],
    "cloud.provider": [
      "digitalocean"
    ],
    "agent.id": [
      "e97f15cc-51d4-47f5-b97c-9b4a008db2ad"
    ],
    "cloud.service.name": [
      "Droplets"
    ],
    "host.containerized": [
      true
    ],
    "ecs.version": [
      "8.0.0"
    ],
    "service.address": [
      "https://autoscale-worker-pool-cnvwt:10250/stats/summary"
    ],
    "agent.version": [
      "8.1.0"
    ],
    "kubernetes.node.network.tx.bytes": [
      183431924
    ],
    "kubernetes.node.fs.inodes.used": [
      167748
    ],
    "kubernetes.node.network.rx.bytes": [
      892901470
    ],
    "host.os.family": [
      "debian"
    ],
    "kubernetes.node.fs.inodes.count": [
      5242880
    ],
    "kubernetes.node.memory.majorpagefaults": [
      132
    ],
    "kubernetes.node.fs.inodes.free": [
      5075132
    ],
    "kubernetes.node.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.node.runtime.imagefs.used.bytes": [
      4135706624
    ],
    "beats_state.state.host.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.labels.beta_kubernetes_io/os": [
      "linux"
    ],
    "kubernetes.labels.kubernetes_io/os": [
      "linux"
    ],
    "kubernetes.node.start_time": [
      "2022-03-17T08:06:03.000Z"
    ],
    "kubernetes.node.memory.pagefaults": [
      50886
    ],
    "kubernetes.node.fs.used.bytes": [
      9759973376
    ],
    "kubernetes.node.memory.rss.bytes": [
      717672448
    ],
    "host.ip": [
      "164.92.151.234",
      "10.18.0.7",
      "fe80::4c0e:efff:fe96:f629",
      "10.110.0.6",
      "fe80::dc05:28ff:fe8c:b521",
      "172.17.0.1",
      "fe80::b45e:11ff:fef8:63c3",
      "10.244.1.41",
      "fe80::5c6a:b5ff:fea2:85aa",
      "fe80::1c62:9eff:fef1:7b28",
      "fe80::bc20:8fff:fefd:d62e",
      "fe80::b433:5ff:feb2:6188",
      "fe80::a0f6:27ff:feff:50c9"
    ],
    "cloud.instance.id": [
      "291075976"
    ],
    "kubernetes.node.cpu.usage.core.ns": [
      1560023696486
    ],
    "agent.type": [
      "metricbeat"
    ],
    "logstash_stats.timestamp": [
      "2022-03-17T11:35:45.664Z"
    ],
    "event.module": [
      "kubernetes"
    ],
    "host.os.kernel": [
      "5.10.0-0.bpo.9-amd64"
    ],
    "kubernetes.node.cpu.usage.nanocores": [
      112841047
    ],
    "beats_state.timestamp": [
      "2022-03-17T11:35:45.664Z"
    ],
    "timestamp": [
      "2022-03-17T11:35:45.664Z"
    ],
    "kubernetes.node.network.tx.errors": [
      0
    ],
    "kubernetes.labels.doks_digitalocean_com/node-pool": [
      "autoscale-worker-pool"
    ],
    "kubernetes.node.memory.usage.bytes": [
      3534573568
    ],
    "kibana_stats.timestamp": [
      "2022-03-17T11:35:45.664Z"
    ],
    "kubernetes.node.fs.available.bytes": [
      71280771072
    ],
    "metricset.period": [
      10000
    ],
    "kubernetes.node.network.rx.errors": [
      0
    ],
    "host.os.codename": [
      "focal"
    ],
    "kubernetes.labels.topology_kubernetes_io/region": [
      "ams3"
    ],
    "kubernetes.node.memory.available.bytes": [
      2619756544
    ],
    "kubernetes.labels.region": [
      "ams3"
    ],
    "metricset.name": [
      "node"
    ],
    "event.duration": [
      962295
    ],
    "kubernetes.node.fs.capacity.bytes": [
      84514574336
    ],
    "kubernetes.node.memory.workingset.bytes": [
      1504595968
    ],
    "@timestamp": [
      "2022-03-17T11:35:45.664Z"
    ],
    "host.os.platform": [
      "ubuntu"
    ],
    "agent.ephemeral_id": [
      "1dab97b7-4f18-4091-b852-d7a7e8e8660c"
    ],
    "beats_state.state.host.architecture": [
      "x86_64"
    ],
    "event.dataset": [
      "kubernetes.node"
    ]
  }
}
```


` metricset.name:"app" `

```
{
  "_index": ".ds-metrics-apm.app.example_nodejs_express-default-2022.03.17-000001",
  "_id": "j_Kul38BchZ5pvpwyU-z",
  "_version": 1,
  "_score": 1,
  "_source": {
    "system.process.cpu.total.norm.pct": 0.004371217215870881,
    "container": {
      "id": "a042a31e66bf9254eb7b3ca021262da65397154a78fa422746328d3e3ce54c2c"
    },
    "kubernetes": {
      "pod": {
        "uid": "a30e5fe0-ee01-4e24-a6fa-651edc1cf79e",
        "name": "example-nodejs-agent-df97f8f7-xz7p5"
      }
    },
    "agent": {
      "name": "nodejs",
      "version": "3.30.0"
    },
    "nodejs.memory.external.bytes": 5768694,
    "system.cpu.total.norm.pct": 0.03732347007397444,
    "observer": {
      "hostname": "quickstart-apm-server-7c4dbdc548-prgbc",
      "id": "372f6018-6c50-4396-a72f-af8c2fc583c8",
      "ephemeral_id": "d5da89fb-f5fe-488d-98ad-a5baa75d3934",
      "type": "apm-server",
      "version": "8.1.0",
      "version_major": 8
    },
    "nodejs.eventloop.delay.avg.ms": 0.157311306233062,
    "system.process.memory.size": 308113408,
    "ecs": {
      "version": "1.12.0"
    },
    "system.process.cpu.system.norm.pct": 0.0015131136516476126,
    "system.memory.actual.free": 2964799488,
    "host": {
      "os": {
        "platform": "linux"
      },
      "ip": [
        "167.99.37.127"
      ],
      "architecture": "x64"
    },
    "system.memory.total": 4124368896,
    "nodejs.memory.heap.used.bytes": 17926712,
    "nodejs.requests.active": 3,
    "event": {
      "agent_id_status": "missing",
      "ingested": "2022-03-17T11:41:28Z"
    },
    "process": {
      "args": [
        "/usr/local/bin/node",
        "/usr/src/app/index.js"
      ],
      "pid": 1,
      "title": "node",
      "ppid": 0
    },
    "nodejs.memory.heap.allocated.bytes": 21430272,
    "data_stream.namespace": "default",
    "nodejs.handles.active": 3,
    "nodejs.memory.arrayBuffers.bytes": 240243,
    "data_stream.type": "metrics",
    "processor": {
      "name": "metric",
      "event": "metric"
    },
    "labels": {
      "hostname": "example-nodejs-agent-df97f8f7-xz7p5",
      "env": "development"
    },
    "metricset.name": "app",
    "system.process.memory.rss.bytes": 62164992,
    "@timestamp": "2022-03-17T11:41:17.854Z",
    "system.process.cpu.user.norm.pct": 0.0028581035642232682,
    "service": {
      "node": {
        "name": "a042a31e66bf9254eb7b3ca021262da65397154a78fa422746328d3e3ce54c2c"
      },
      "environment": "development",
      "framework": {
        "name": "express",
        "version": "4.17.3"
      },
      "name": "example-nodejs-express",
      "runtime": {
        "name": "node",
        "version": "16.13.2"
      },
      "language": {
        "name": "javascript"
      }
    },
    "data_stream.dataset": "apm.app.example_nodejs_express"
  },
  "fields": {
    "system.process.cpu.total.norm.pct": [
      0.004
    ],
    "process.pid": [
      1
    ],
    "service.language.name": [
      "javascript"
    ],
    "container.id": [
      "a042a31e66bf9254eb7b3ca021262da65397154a78fa422746328d3e3ce54c2c"
    ],
    "system.process.memory.size": [
      308113408
    ],
    "system.process.cpu.system.norm.pct": [
      0.0015131136
    ],
    "processor.event": [
      "metric"
    ],
    "system.memory.actual.free": [
      2964799488
    ],
    "agent.name": [
      "nodejs"
    ],
    "system.memory.total": [
      4124368896
    ],
    "nodejs.memory.heap.used.bytes": [
      17926712
    ],
    "nodejs.requests.active": [
      3
    ],
    "event.agent_id_status": [
      "missing"
    ],
    "process.ppid": [
      0
    ],
    "nodejs.memory.heap.allocated.bytes": [
      21430272
    ],
    "processor.name": [
      "metric"
    ],
    "service.runtime.version": [
      "16.13.2"
    ],
    "data_stream.type": [
      "metrics"
    ],
    "host.architecture": [
      "x64"
    ],
    "observer.id": [
      "372f6018-6c50-4396-a72f-af8c2fc583c8"
    ],
    "observer.version": [
      "8.1.0"
    ],
    "observer.type": [
      "apm-server"
    ],
    "ecs.version": [
      "1.12.0"
    ],
    "agent.version": [
      "3.30.0"
    ],
    "process.title": [
      "node"
    ],
    "service.framework.version": [
      "4.17.3"
    ],
    "nodejs.memory.external.bytes": [
      5768694
    ],
    "service.node.name": [
      "a042a31e66bf9254eb7b3ca021262da65397154a78fa422746328d3e3ce54c2c"
    ],
    "kubernetes.pod.uid": [
      "a30e5fe0-ee01-4e24-a6fa-651edc1cf79e"
    ],
    "system.cpu.total.norm.pct": [
      0.037
    ],
    "host.ip": [
      "167.99.37.127"
    ],
    "nodejs.eventloop.delay.avg.ms": [
      0.1573113
    ],
    "labels.env": [
      "development"
    ],
    "kubernetes.pod.name": [
      "example-nodejs-agent-df97f8f7-xz7p5"
    ],
    "service.environment": [
      "development"
    ],
    "service.name": [
      "example-nodejs-express"
    ],
    "service.framework.name": [
      "express"
    ],
    "data_stream.namespace": [
      "default"
    ],
    "service.runtime.name": [
      "node"
    ],
    "process.args": [
      "/usr/local/bin/node",
      "/usr/src/app/index.js"
    ],
    "nodejs.handles.active": [
      3
    ],
    "observer.version_major": [
      8
    ],
    "nodejs.memory.arrayBuffers.bytes": [
      240243
    ],
    "observer.hostname": [
      "quickstart-apm-server-7c4dbdc548-prgbc"
    ],
    "system.process.memory.rss.bytes": [
      62164992
    ],
    "metricset.name": [
      "app"
    ],
    "event.ingested": [
      "2022-03-17T11:41:28.000Z"
    ],
    "@timestamp": [
      "2022-03-17T11:41:17.854Z"
    ],
    "observer.ephemeral_id": [
      "d5da89fb-f5fe-488d-98ad-a5baa75d3934"
    ],
    "host.os.platform": [
      "linux"
    ],
    "system.process.cpu.user.norm.pct": [
      0.0028581035
    ],
    "data_stream.dataset": [
      "apm.app.example_nodejs_express"
    ],
    "labels.hostname": [
      "example-nodejs-agent-df97f8f7-xz7p5"
    ]
  }
}
```

` metricset.name:"container"  `

```
{
  "_index": ".ds-metricbeat-8.1.0-2022.03.17-000001",
  "_id": "vwLxl38BBI8EJq6VhVAj",
  "_version": 1,
  "_score": 1,
  "_source": {
    "@timestamp": "2022-03-17T12:54:21.222Z",
    "kubernetes": {
      "pod": {
        "ip": "10.244.1.88",
        "name": "example-next-rum-9559dc797-tkgzx",
        "uid": "dc2e9704-817e-408b-9e7b-ea359d07811b"
      },
      "namespace": "default",
      "replicaset": {
        "name": "example-next-rum-9559dc797"
      },
      "labels": {
        "example-agents": "external",
        "pod-template-hash": "9559dc797"
      },
      "deployment": {
        "name": "example-next-rum"
      },
      "container": {
        "start_time": "2022-03-17T08:50:33Z",
        "name": "example-agent-next-rum",
        "cpu": {
          "usage": {
            "core": {
              "ns": 3123765703
            },
            "node": {
              "pct": 0
            },
            "limit": {
              "pct": 0
            },
            "nanocores": 0
          }
        },
        "memory": {
          "usage": {
            "limit": {
              "pct": 0.1304779052734375
            },
            "bytes": 70049792,
            "node": {
              "pct": 0.01698443374958537
            }
          },
          "workingset": {
            "bytes": 63627264,
            "limit": {
              "pct": 0.1185150146484375
            }
          },
          "rss": {
            "bytes": 62873600
          },
          "pagefaults": 75768,
          "majorpagefaults": 0,
          "available": {
            "bytes": 473243648
          }
        },
        "rootfs": {
          "capacity": {
            "bytes": 84514574336
          },
          "used": {
            "bytes": 1843200
          },
          "inodes": {
            "used": 24
          },
          "available": {
            "bytes": 71272947712
          }
        },
        "logs": {
          "available": {
            "bytes": 71272947712
          },
          "capacity": {
            "bytes": 84514574336
          },
          "used": {
            "bytes": 4096
          },
          "inodes": {
            "count": 5242880,
            "used": 1,
            "free": 5075132
          }
        }
      },
      "node": {
        "hostname": "autoscale-worker-pool-cnvwt",
        "name": "autoscale-worker-pool-cnvwt",
        "uid": "576c6ad7-da1f-407e-bb62-500e582302bd",
        "labels": {
          "node_kubernetes_io/instance-type": "s-2vcpu-4gb",
          "beta_kubernetes_io/arch": "amd64",
          "beta_kubernetes_io/instance-type": "s-2vcpu-4gb",
          "topology_kubernetes_io/region": "ams3",
          "doks_digitalocean_com/node-id": "d5819820-b4ef-4deb-83d9-bb8e049ea2cf",
          "kubernetes_io/os": "linux",
          "doks_digitalocean_com/node-pool": "autoscale-worker-pool",
          "doks_digitalocean_com/node-pool-id": "f68680a4-c692-4057-84c0-710141e70d1e",
          "failure-domain_beta_kubernetes_io/region": "ams3",
          "kubernetes_io/hostname": "autoscale-worker-pool-cnvwt",
          "beta_kubernetes_io/os": "linux",
          "doks_digitalocean_com/version": "1.22.7-do.0",
          "kubernetes_io/arch": "amd64",
          "region": "ams3"
        }
      },
      "namespace_uid": "1baea3a9-dcd6-454f-935d-86a1a007c60d",
      "namespace_labels": {
        "kubernetes_io/metadata_name": "default"
      }
    },
    "agent": {
      "name": "autoscale-worker-pool-cnvwt",
      "type": "metricbeat",
      "version": "8.1.0",
      "ephemeral_id": "1dab97b7-4f18-4091-b852-d7a7e8e8660c",
      "id": "e97f15cc-51d4-47f5-b97c-9b4a008db2ad"
    },
    "container": {
      "id": "19661b2498f45a1b373999a6a98356784f949c89d2e29f8c688ad96c44c5e56e",
      "runtime": "containerd"
    },
    "service": {
      "address": "https://autoscale-worker-pool-cnvwt:10250/stats/summary",
      "type": "kubernetes"
    },
    "ecs": {
      "version": "8.0.0"
    },
    "host": {
      "hostname": "autoscale-worker-pool-cnvwt",
      "architecture": "x86_64",
      "name": "autoscale-worker-pool-cnvwt",
      "os": {
        "version": "20.04.3 LTS (Focal Fossa)",
        "family": "debian",
        "name": "Ubuntu",
        "kernel": "5.10.0-0.bpo.9-amd64",
        "codename": "focal",
        "type": "linux",
        "platform": "ubuntu"
      },
      "containerized": true,
      "ip": [
        "164.92.151.234",
        "10.18.0.7",
        "fe80::4c0e:efff:fe96:f629",
        "10.110.0.6",
        "fe80::dc05:28ff:fe8c:b521",
        "172.17.0.1",
        "fe80::b45e:11ff:fef8:63c3",
        "10.244.1.41",
        "fe80::5c6a:b5ff:fea2:85aa",
        "fe80::1c62:9eff:fef1:7b28",
        "fe80::bc20:8fff:fefd:d62e",
        "fe80::b433:5ff:feb2:6188",
        "fe80::a0f6:27ff:feff:50c9"
      ],
      "mac": [
        "4e:0e:ef:96:f6:29",
        "de:05:28:8c:b5:21",
        "02:42:60:f4:7c:07",
        "b6:5e:11:f8:63:c3",
        "5e:6a:b5:a2:85:aa",
        "1e:62:9e:f1:7b:28",
        "be:20:8f:fd:d6:2e",
        "b6:33:05:b2:61:88",
        "a2:f6:27:ff:50:c9"
      ]
    },
    "cloud": {
      "region": "ams3",
      "service": {
        "name": "Droplets"
      },
      "instance": {
        "id": "291075976"
      },
      "provider": "digitalocean"
    },
    "metricset": {
      "name": "container",
      "period": 10000
    },
    "event": {
      "dataset": "kubernetes.container",
      "module": "kubernetes",
      "duration": 2013184
    }
  },
  "fields": {
    "kubernetes.node.uid": [
      "576c6ad7-da1f-407e-bb62-500e582302bd"
    ],
    "kubernetes.namespace_uid": [
      "1baea3a9-dcd6-454f-935d-86a1a007c60d"
    ],
    "kubernetes.deployment.name": [
      "example-next-rum"
    ],
    "host.os.name.text": [
      "Ubuntu"
    ],
    "kubernetes.container.logs.inodes.used": [
      1
    ],
    "host.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.node.labels.kubernetes_io/os": [
      "linux"
    ],
    "host.mac": [
      "4e:0e:ef:96:f6:29",
      "de:05:28:8c:b5:21",
      "02:42:60:f4:7c:07",
      "b6:5e:11:f8:63:c3",
      "5e:6a:b5:a2:85:aa",
      "1e:62:9e:f1:7b:28",
      "be:20:8f:fd:d6:2e",
      "b6:33:05:b2:61:88",
      "a2:f6:27:ff:50:c9"
    ],
    "container.id": [
      "19661b2498f45a1b373999a6a98356784f949c89d2e29f8c688ad96c44c5e56e"
    ],
    "kubernetes.labels.pod-template-hash": [
      "9559dc797"
    ],
    "service.type": [
      "kubernetes"
    ],
    "kubernetes.container.cpu.usage.core.ns": [
      3123765703
    ],
    "host.os.version": [
      "20.04.3 LTS (Focal Fossa)"
    ],
    "kubernetes.node.labels.beta_kubernetes_io/os": [
      "linux"
    ],
    "kubernetes.namespace": [
      "default"
    ],
    "kubernetes.container.memory.usage.limit.pct": [
      0.13
    ],
    "host.os.name": [
      "Ubuntu"
    ],
    "agent.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "host.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.container.memory.usage.node.pct": [
      0.017
    ],
    "kubernetes.node.labels.topology_kubernetes_io/region": [
      "ams3"
    ],
    "beats_state.state.host.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.node.labels.doks_digitalocean_com/version": [
      "1.22.7-do.0"
    ],
    "cloud.region": [
      "ams3"
    ],
    "host.os.type": [
      "linux"
    ],
    "kubernetes.container.memory.available.bytes": [
      473243648
    ],
    "kubernetes.container.logs.inodes.count": [
      5242880
    ],
    "kubernetes.container.logs.available.bytes": [
      71272947712
    ],
    "kubernetes.container.cpu.usage.node.pct": [
      0
    ],
    "kubernetes.container.memory.pagefaults": [
      75768
    ],
    "agent.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "host.architecture": [
      "x86_64"
    ],
    "kubernetes.container.cpu.usage.limit.pct": [
      0
    ],
    "cloud.provider": [
      "digitalocean"
    ],
    "container.runtime": [
      "containerd"
    ],
    "agent.id": [
      "e97f15cc-51d4-47f5-b97c-9b4a008db2ad"
    ],
    "cloud.service.name": [
      "Droplets"
    ],
    "host.containerized": [
      true
    ],
    "ecs.version": [
      "8.0.0"
    ],
    "service.address": [
      "https://autoscale-worker-pool-cnvwt:10250/stats/summary"
    ],
    "kubernetes.node.labels.beta_kubernetes_io/instance-type": [
      "s-2vcpu-4gb"
    ],
    "kubernetes.container.rootfs.available.bytes": [
      71272947712
    ],
    "kubernetes.container.memory.rss.bytes": [
      62873600
    ],
    "kubernetes.container.memory.workingset.bytes": [
      63627264
    ],
    "agent.version": [
      "8.1.0"
    ],
    "host.os.family": [
      "debian"
    ],
    "kubernetes.node.name": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.container.logs.inodes.free": [
      5075132
    ],
    "kubernetes.node.labels.failure-domain_beta_kubernetes_io/region": [
      "ams3"
    ],
    "beats_state.state.host.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.node.hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "kubernetes.node.labels.region": [
      "ams3"
    ],
    "kubernetes.node.labels.doks_digitalocean_com/node-id": [
      "d5819820-b4ef-4deb-83d9-bb8e049ea2cf"
    ],
    "kubernetes.node.labels.doks_digitalocean_com/node-pool-id": [
      "f68680a4-c692-4057-84c0-710141e70d1e"
    ],
    "kubernetes.node.labels.node_kubernetes_io/instance-type": [
      "s-2vcpu-4gb"
    ],
    "kubernetes.node.labels.doks_digitalocean_com/node-pool": [
      "autoscale-worker-pool"
    ],
    "kubernetes.pod.uid": [
      "dc2e9704-817e-408b-9e7b-ea359d07811b"
    ],
    "kubernetes.container.memory.usage.bytes": [
      70049792
    ],
    "kubernetes.container.logs.used.bytes": [
      4096
    ],
    "kubernetes.container.rootfs.used.bytes": [
      1843200
    ],
    "kubernetes.container.start_time": [
      "2022-03-17T08:50:33.000Z"
    ],
    "kubernetes.container.cpu.usage.nanocores": [
      0
    ],
    "host.ip": [
      "164.92.151.234",
      "10.18.0.7",
      "fe80::4c0e:efff:fe96:f629",
      "10.110.0.6",
      "fe80::dc05:28ff:fe8c:b521",
      "172.17.0.1",
      "fe80::b45e:11ff:fef8:63c3",
      "10.244.1.41",
      "fe80::5c6a:b5ff:fea2:85aa",
      "fe80::1c62:9eff:fef1:7b28",
      "fe80::bc20:8fff:fefd:d62e",
      "fe80::b433:5ff:feb2:6188",
      "fe80::a0f6:27ff:feff:50c9"
    ],
    "cloud.instance.id": [
      "291075976"
    ],
    "agent.type": [
      "metricbeat"
    ],
    "logstash_stats.timestamp": [
      "2022-03-17T12:54:21.222Z"
    ],
    "event.module": [
      "kubernetes"
    ],
    "host.os.kernel": [
      "5.10.0-0.bpo.9-amd64"
    ],
    "kubernetes.labels.example-agents": [
      "external"
    ],
    "kubernetes.container.rootfs.inodes.used": [
      24
    ],
    "kubernetes.pod.name": [
      "example-next-rum-9559dc797-tkgzx"
    ],
    "beats_state.timestamp": [
      "2022-03-17T12:54:21.222Z"
    ],
    "timestamp": [
      "2022-03-17T12:54:21.222Z"
    ],
    "kubernetes.pod.ip": [
      "10.244.1.88"
    ],
    "kubernetes.container.memory.majorpagefaults": [
      0
    ],
    "kubernetes.container.name": [
      "example-agent-next-rum"
    ],
    "kibana_stats.timestamp": [
      "2022-03-17T12:54:21.222Z"
    ],
    "kubernetes.replicaset.name": [
      "example-next-rum-9559dc797"
    ],
    "metricset.period": [
      10000
    ],
    "host.os.codename": [
      "focal"
    ],
    "kubernetes.container.rootfs.capacity.bytes": [
      84514574336
    ],
    "kubernetes.namespace_labels.kubernetes_io/metadata_name": [
      "default"
    ],
    "kubernetes.node.labels.kubernetes_io/hostname": [
      "autoscale-worker-pool-cnvwt"
    ],
    "metricset.name": [
      "container"
    ],
    "kubernetes.container.memory.workingset.limit.pct": [
      0.119
    ],
    "event.duration": [
      2013184
    ],
    "kubernetes.node.labels.beta_kubernetes_io/arch": [
      "amd64"
    ],
    "kubernetes.container.logs.capacity.bytes": [
      84514574336
    ],
    "@timestamp": [
      "2022-03-17T12:54:21.222Z"
    ],
    "host.os.platform": [
      "ubuntu"
    ],
    "agent.ephemeral_id": [
      "1dab97b7-4f18-4091-b852-d7a7e8e8660c"
    ],
    "kubernetes.node.labels.kubernetes_io/arch": [
      "amd64"
    ],
    "beats_state.state.host.architecture": [
      "x86_64"
    ],
    "event.dataset": [
      "kubernetes.container"
    ]
  }
}
```
