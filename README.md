# Cost profiling microservices applications using an APM stack

Sjouke de Vries <sup>1,2</sup>, Frank Blaauw <sup>1</sup> , and Vasilios Andrikopoulos<sup>2*</sup>

<sup>1</sup> Researchable B  ̇V ̇, Groningen, the Netherlands; s.de.vries@researchable.nl, f.j.blaauw@researchable.nl
<sup>2</sup> University of Groningen, Groningen, the Netherlands; v.andrikopoulos@rug.nl
<sup>*</sup> Correspondence: v.andrikopoulos@rug.nl

Example that deploys the Elastic observability stack. 

> <b>Abstract</b>: Understanding how the different parts of a cloud-native application contribute to its operating expenses is an important step towards optimizing this cost. However, with the adoption and rollout of microservices architectures, the gathering of the necessary data becomes much more involved and nuanced. Existing solutions for this purpose are either closed-source and proprietary, or focus only on the infrastructural footprint of the applications. In response to that, in this work we present a cost profiling solution aimed at microservices applications, building on a popular open-source Application Performance Monitoring (APM) stack. By means of a case study with a data engineering company, we demonstrate how our proposed solution can provide deeper insights into the cost profile of the various application components, and drive informed decision-making in managing the deployment of the application.


<img src="architecture/apm-architecture.png" alt="Elastic APM Architecture"  width="960" height="480">

<!-- The Elastic APM also supports OpenTelemetry agents:
![APM OpenTelemtry](architecture/open-telemetry-protocol-arch.png "APM OpenTelemtry") -->

## Getting started

### Create the infrastructure

```
cd terraform
cp terraform.tfvars.example terraform.tfvars
terraform apply
```

or create a [Minikube](https://minikube.sigs.k8s.io/docs/) cluster with `minikube start --cpus 2 --memory 8GB`. There is documentation about accessing minikube apps [here](https://minikube.sigs.k8s.io/docs/handbook/accessing/). 

### Auxiliary resources
```
# Ingress-nginx
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm upgrade my-ingress-nginx ingress-nginx/ingress-nginx --version 4.4.0 --namespace ingress-nginx --create-namespace --install --values=./charts/monitoring-stack/ingress-nginx-values.yaml


# Cert-manager
helm repo add cert-manager https://charts.jetstack.io
helm upgrade my-cert-manager cert-manager/cert-manager --version 1.10.1 --namespace cert-manager --create-namespace --install --values=./charts/monitoring-stack/cert-manager-values.yaml

# Elastic operator
helm repo add elastic https://helm.elastic.co
helm upgrade my-eck-operator elastic/eck-operator --version 2.5.0 --namespace monitoring --create-namespace --install
```

### Elastic stack
```
helm upgrade monitoring-stack ./charts/monitoring-stack --values=./charts/monitoring-stack/values.yaml --namespace monitoring --create-namespace --install
```
### (Optional) example applications
```
helm upgrade example-app ./charts/target --values=./charts/target/values.yaml --install --namespace application --create-namespace
```

## Testing
### Running example microservices
The data is collected with [Elastic's APM agents](https://www.elastic.co/guide/en/apm/agent/index.html) 
1. Navigate to the folder with one of the example agents: `cd example-apm-agents` and e.g. `cd node-express`
2. Optionally change the environment variables to configure the agent or `./run.sh`
3. Visit `http://localhost:3000` and `http://localhost:3000/error`, data will be sent to the APM server

### Load testing
You can use [Locust](https://locust.io/) for load testing. Replace the host with your own.

```
locust --headless --users 50 --spawn-rate 3 -H https://agents.domain.dev
```
