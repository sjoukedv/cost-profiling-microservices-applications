data "digitalocean_kubernetes_versions" "latest" {}

resource "digitalocean_kubernetes_cluster" "monitoring_cluster" {
  name    = "monitoring"
  region  = "ams3"
  version = data.digitalocean_kubernetes_versions.latest.latest_version
  ha      = true

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-4vcpu-8gb"
    auto_scale = true
    min_nodes  = 2
    max_nodes  = 5
  }
}
