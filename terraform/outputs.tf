# Output raw kube_config for use of kubectl
output "kube_config" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.monitoring_cluster.kube_config.0.raw_config
}

resource "local_file" "kubeconfigdo" {
  content  = digitalocean_kubernetes_cluster.monitoring_cluster.kube_config.0.raw_config
  filename = "${path.module}/kubeconfig_do"
}
