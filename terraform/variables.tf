variable "DO_TOKEN" {
  type        = string
  description = "DigitalOcean API Token"
}

variable "DO_SPACES_ACCESS_ID" {
  type        = string
  description = "DigitalOcean Spaces Key ID"
}

variable "DO_SPACES_SECRET_KEY" {
  type        = string
  description = "DigitalOcean Spaces Key Secret"
}
