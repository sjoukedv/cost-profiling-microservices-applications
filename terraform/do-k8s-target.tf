# This is your client's cluster 
resource "digitalocean_kubernetes_cluster" "target_cluster" {
  name    = "target"
  region  = "ams3"
  version = data.digitalocean_kubernetes_versions.latest.latest_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-4vcpu-8gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 3
  }
}
